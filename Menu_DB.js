/**
 * CƠ SỞ DỮ LIỆU QUẢN LÝ DANH SÁCH Menu
*/

// Khởi tạo mảng gMenuDb gán dữ liêu từ server trả về 
var gMenuDb = {
  menu: [],

  // order methods
  // function get Menu index form Menu id
  // get Menu index from Menu id
  getIndexFromMenuId: function (paramMenuId) {
    var vMenuIndex = -1;
    var vMenuFound = false;
    var vLoopIndex = 0;
    while (!vMenuFound && vLoopIndex < this.menu.length) {
      if (this.menu[vLoopIndex].id === paramMenuId) {
        vMenuIndex = vLoopIndex;
        vMenuFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vMenuIndex;
  },

  // hàm show order obj lên modal
  showMenuDataToForm: function (paramRequsetData) {
   
    $("#inp-kich-co").val(paramRequsetData.kichCo);
    $("#inp-duong-kinh").val(paramRequsetData.duongKinh);
    $("#inp-suon").val(paramRequsetData.suon);
    $("#inp-salad").val(paramRequsetData.salad);
    $("#inp-so-luong-nuoc-uong").val(paramRequsetData.soLuongNuocUong);
    $("#inp-thanh-tien").val(paramRequsetData.thanhTien);
  },


  
};

