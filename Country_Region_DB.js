/**
 * CƠ SỞ DỮ LIỆU QUẢN LÝ DANH SÁCH COUNTRY
*/

// Khởi tạo mảng gCountryDb gán dữ liêu từ server trả về 
var gCountryDb = {
  countries: [],

  // country methods
  // function get COUNTRY index form COUNTRY id
  // get COUNTRY index from COUNTRY id
  getIndexFromCountryId: function (paramCountryId) {
    var vCountryIndex = -1;
    var vCountryFound = false;
    var vLoopIndex = 0;
    while (!vCountryFound && vLoopIndex < this.countries.length) {
      if (this.countries[vLoopIndex].id === paramCountryId) {
        vCountryIndex = vLoopIndex;
        vCountryFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vCountryIndex;
  },

  // hàm show country obj lên form
  showCountryDataToForm: function (paramRequsetData) {
   
    $("#inp-country-code").val(paramRequsetData.countryCode);
    $("#inp-country-name").val(paramRequsetData.countryName);
  },
  
};
/**
 * CƠ SỞ DỮ LIỆU QUẢN LÝ DANH SÁCH REGION
*/

// Khởi tạo mảng gRegionDb gán dữ liêu từ server trả về 
var gRegionDb = {
  regions: [],

  // region methods
  // function get REGION index form REGION id
  // get REGION index from REGION id
  getIndexFromRegionId: function (paramRegionId) {
    var vRegionIndex = -1;
    var vRegionFound = false;
    var vLoopIndex = 0;
    while (!vRegionFound && vLoopIndex < this.regions.length) {
      if (this.regions[vLoopIndex].id === paramRegionId) {
        vRegionIndex = vLoopIndex;
        vRegionFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vRegionIndex;
  },

  // hàm show region obj lên form
  showRegionDataToForm: function (paramRequsetData) {
   
    $("#inp-update-region-code").val(paramRequsetData.regionCode);
    $("#inp-update-region-name").val(paramRequsetData.regionName);
  },
  
};
