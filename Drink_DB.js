/**
 * CƠ SỞ DỮ LIỆU QUẢN LÝ DANH SÁCH DRINK
*/

// Khởi tạo mảng gDrinkDb gán dữ liêu từ server trả về 
var gDrinkDb = {
  drink: [],

  // drink methods
  // function get drink index form drink id
  // get drink index from drink id
  getIndexFromDrinkId: function (paramDrinkId) {
    var vDrinkIndex = -1;
    var vDrinkFound = false;
    var vLoopIndex = 0;
    while (!vDrinkFound && vLoopIndex < this.drink.length) {
      if (this.drink[vLoopIndex].id === paramDrinkId) {
        vDrinkIndex = vLoopIndex;
        vDrinkFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vDrinkIndex;
  },

  // hàm show drink obj lên modal
  showDrinkDataToForm: function (paramRequsetData) {
   
    $("#inp-ma-nuoc-uong").val(paramRequsetData.maNuocUong);
    $("#inp-ten-nuoc-uong").val(paramRequsetData.tenNuocUong);
    $("#inp-don-gia").val(paramRequsetData.donGia);
    $("#inp-ghi-chu").val(paramRequsetData.ghiChu);
    $("#inp-ngay-tao").val(paramRequsetData.ngayTao);
    $("#inp-ngay-cap-nhat").val(paramRequsetData.ngayCapNhat);
  },


  
};

