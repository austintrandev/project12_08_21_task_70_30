/**
 * CƠ SỞ DỮ LIỆU QUẢN LÝ DANH SÁCH USER
*/

// Khởi tạo mảng gUserDb gán dữ liêu từ server trả về 
var gUserDb = {
  users: [],

  // user methods
  // function get user index form user id
  // get user index from user id
  getIndexFromUserId: function (paramUserId) {
    var vUserIndex = -1;
    var vUserFound = false;
    var vLoopIndex = 0;
    while (!vUserFound && vLoopIndex < this.users.length) {
      if (this.users[vLoopIndex].id === paramUserId) {
        vUserIndex = vLoopIndex;
        vUserFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vUserIndex;
  },

  // hàm show user obj lên form
  showUserDataToForm: function (paramRequsetData) {
   
    $("#inp-full-name").val(paramRequsetData.fullName);
    $("#inp-email").val(paramRequsetData.email);
    $("#inp-phone").val(paramRequsetData.phone);
    $("#inp-address").val(paramRequsetData.address);
  },
  
};
/**
 * CƠ SỞ DỮ LIỆU QUẢN LÝ DANH SÁCH ORDER
*/

// Khởi tạo mảng gOrderDb gán dữ liêu từ server trả về 
var gOrderDb = {
  orders: [],

  // order methods
  // function get order index form order id
  // get order index from order id
  getIndexFromOrderId: function (paramOrderId) {
    var vOrderIndex = -1;
    var vOrderFound = false;
    var vLoopIndex = 0;
    while (!vOrderFound && vLoopIndex < this.orders.length) {
      if (this.orders[vLoopIndex].id === paramOrderId) {
        vOrderIndex = vLoopIndex;
        vOrderFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vOrderIndex;
  },

  // hàm show region obj lên form
  showOrderDataToForm: function (paramRequesttData) {
   
    $("#inp-update-order-code").val(paramRequesttData.orderCode);
    $("#inp-update-pizza-type").val(paramRequesttData.pizzaType);
    $("#inp-update-pizza-size").val(paramRequesttData.pizzaSize);
    $("#inp-update-voucher-code").val(paramRequesttData.voucherCode);
    $("#inp-update-price").val(paramRequesttData.price);
    $("#inp-update-paid").val(paramRequesttData.paid);
  },
  
};
