package com.pizza365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pizza365CMenuDrinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(Pizza365CMenuDrinkApplication.class, args);
	}

}
