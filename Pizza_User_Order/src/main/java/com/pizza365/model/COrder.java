package com.pizza365.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pizza365.model.*;

@Entity
@Table(name = "orders")
public class COrder {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "order_code", unique = true)
	private String orderCode;

	@Column(name = "pizza_size")
	private String pizzaSize;

	@Column(name = "pizza_type")
	private String pizzaType;

	@Column(name = "voucher_code")
	private String voucherCode;

	@Column(name = "price")
	private long price;

	@Column(name = "paid")
	private long paid;

	@ManyToOne
	@JsonIgnore
	private CUser user;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getPizzaSize() {
		return pizzaSize;
	}

	public void setPizzaSize(String pizzaSize) {
		this.pizzaSize = pizzaSize;
	}

	public String getPizzaType() {
		return pizzaType;
	}

	public void setPizzaType(String pizzaType) {
		this.pizzaType = pizzaType;
	}

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public long getPaid() {
		return paid;
	}

	public void setPaid(long paid) {
		this.paid = paid;
	}

	public CUser getUser() {
		return user;
	}

	public void setUser(CUser user) {
		this.user = user;
	}
}
